//ANEXO 1 PREPROCESAMIENTO CON LENGUAJE JAVASCRIPT Y GOOGLE EARRTH ENGINE

// Calculate NDVI and NDBI from Recent Sentinel Satellite Imagery in Google Earth Engine
//Author: Annie Taylor
//Modified by Metzi Aguilar
// Last Modified: January 2023
var tecla = ee.FeatureCollection("users/maguilar/municipios").filter(ee.Filter.eq('NA2', 'SANTA TECLA'));
    	 
//correccion nubes
function maskS2clouds(image) {
  var qa = image.select('QA60')

  // Bits 10 and 11 are clouds and cirrus, respectively.
  var cloudBitMask = 1 << 10;
  var cirrusBitMask = 1 << 11;

  // Both flags should be set to zero, indicating clear conditions.
  var mask = qa.bitwiseAnd(cloudBitMask).eq(0).and(
         	qa.bitwiseAnd(cirrusBitMask).eq(0))

  // Return the masked and scaled data, without the QA bands.
  return image.updateMask(mask).divide(10000)
  	.select("B.*")
  	.copyProperties(image, ["system:time_start"])
}

// Import all available Sentinel 2 surface reflectance imagery as an ee.ImageCollection
// and filter for images within your study area
var S2_SR = ee.ImageCollection('COPERNICUS/S2_SR')
.filter(ee.Filter.date('2021-01-01', '2021-01-31'))
.select('B4', 'B8', 'B11','QA60')
.filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 20))
.map(maskS2clouds)
.map(function(image){return image.clip(tecla)});

// Center your map at your study area
Map.setCenter(-89.191389, 13.698889, 8);

// Create a function that adds an NDVI band to a Sentinel-2 image
var addNDVI = function(image) {
  var ndvi = image.normalizedDifference(['B8', 'B4']).rename('NDVI');
  return image.addBands(ndvi);
};

// Create a function that adds an NDBI band to a Sentinel-2 image

var addNDBI = function(image) {
  var ndbi = image.normalizedDifference(['B11', 'B8']).rename('NDBI');
  return image.addBands(ndbi);
};

// Apply this function across your image collection
var S2_NDVI = S2_SR.map(addNDVI);
var S2_NDBI = S2_SR.map(addNDBI);

// Create a variable with the NDVI color scheme/palette
var NDVIpalette = ['FFFFFF', 'CE7E45', 'DF923D', 'F1B555', 'FCD163', '99B718',
                	'74A901', '66A000', '529400', '3E8601', '207401', '056201',
                	'004C00', '023B01', '012E01', '011D01', '011301'];

// Select just the NDVI band and add the NDVI image to the map using the NDVI color scheme
Map.addLayer(S2_NDVI.select('NDVI'), {palette: NDVIpalette}, 'Recent Sentinel NDVI');
Map.addLayer(S2_NDBI.select('NDBI'), {palette: NDVIpalette}, 'Recent Sentinel NDBI');

//Export images to google drive
Export.image.toDrive({
  image: S2_NDVI.select('NDVI').median() ,
  description: 'NDVI_Tecla',
  scale: 10,
  region: tecla,
  fileFormat: 'GeoTIFF',
  formatOptions: {
	cloudOptimized: true
  }});
 
  Export.image.toDrive({
  image: S2_NDBI.select('NDBI').median() ,
  description: 'NDBI_Tecla',
  scale: 10,
  region: tecla,
  fileFormat: 'GeoTIFF',
  formatOptions: {
	cloudOptimized: true
  }});
